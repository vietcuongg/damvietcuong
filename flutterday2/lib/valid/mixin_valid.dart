mixin Validation {
  String? ValidationEmail(String? value) {
    if (value!.isEmpty) {
      return "Email is required";
    } else if (!value.contains('@')) {
      return "Your email missing'@'";
    } else if (!value.contains('.')) {
      return "Your email Must have '.'";
    } else if (!value.contains('com')) {
      return "Your email must have 'com'";
    }
  }

  String? ValidationFirstname(String? value) {
    if (value!.isEmpty) {
      return "You must enter your name";
    } else if (!value.contains('!@#^&*()_+*/-')) {
      return "You must not enter this";
    }
  }

  String? ValidationName(String? value) {
    if (value!.isEmpty) {
      return "You must enter your name";
    } else if (!value.contains('!@#^&*()_+*/-')) {
      return "You must not enter this";
    }
  }

  String? ValidAddress(String? value) {
    if (value!.isEmpty) {
      return "You must enter your Address";
    }
  }
}
String? ValidationBirthday(String? value) {
  if (value!.isEmpty) {
    return "You must enter your birthday";
  }
}
