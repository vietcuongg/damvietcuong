import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutterday2/valid/mixin_valid.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login me',
      home: Scaffold(
        body: LoginScreen(),
      ),
    );
  }
}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<StatefulWidget> with Validation {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String lname;
  late String fname;
  late String byear;
  late String address;
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(20.0),
        child: Form(
            key: formKey,
            child: Column(
              children: [
                emailField(),
                fistnameField(),
                nameField(),
                birthdayField(),
                addressField(),
                loginButton(),
              ],
            )));
  }

  Widget emailField() {
    return TextFormField(
        decoration: InputDecoration(labelText: "Email address"),
        validator: ValidationEmail,
        onSaved: (value) {
          print('OnSaved: value=$value');
        });
  }

  Widget fistnameField() {
    return TextFormField(
        validator: ValidationFirstname,
        decoration: InputDecoration(labelText: "FirstName:"),
        onSaved: (value) {
          print('OnSaved: value =$value');
        });
  }

  Widget nameField() {
    return TextFormField(
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.allow(RegExp('(a-zA-Z)'))
        ],
        decoration: InputDecoration(labelText: "Name"),
        validator: ValidationName,
        onSaved: (value) {
          print('OnSaved: value=$value');
        });
  }

  Widget addressField() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Address"),
    );
  }

  Widget birthdayField() {
    return TextFormField(
        keyboardType: TextInputType.number,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.digitsOnly
        ],
        decoration: InputDecoration(labelText: "Birthday"),
        validator: ValidationBirthday,
        onSaved: (value) {
          print('OnSave value =$value');
        });
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();
          }
        },
        child: Text('Submit'));
  }
}
