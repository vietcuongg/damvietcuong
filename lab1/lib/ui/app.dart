import 'package:flutter/material.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }
}

class AppState extends State<App> {
  int counter = 0;

  fetchImage() {
    print('Hi there: counter = $counter');
    setState(() {
      counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    var appWidget = new MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Image Viewer - v0.0.7'),
        ),
        body: Text('Display list of images here, counter = $counter'),
        floatingActionButton:
            FloatingActionButton(child: Icon(Icons.add), onPressed: fetchImage),
      ),
    );
    return appWidget;
  }
}
